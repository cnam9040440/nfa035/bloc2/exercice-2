package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.BadgeDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64StreamingImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class MultiBadgeWalletDAOImpl  implements BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(SingleBadgeWalletDAOImpl.class);

    private File walletDatabase;
    private ResumableImageFileFrame media;

    private ImageStreamingDeserializer<ResumableImageFrameMedia> deserializer;

    public MultiBadgeWalletDAOImpl(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
        this.media = new ResumableImageFileFrame(walletDatabase);
    }
    /**
     * {@inheritDoc}
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException {
        ImageSerializerBase64DatabaseImpl serializer = new ImageSerializerBase64DatabaseImpl();
        serializer.serialize(image, media);
    }

    /**
     * {@inheritDoc}
     * @param imageStream
     * @throws IOException
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException {
        BadgeDeserializer deserializer = new ImageDeserializerBase64DatabaseImpl(imageStream);
        deserializer.deserialize(media);
    }
}
