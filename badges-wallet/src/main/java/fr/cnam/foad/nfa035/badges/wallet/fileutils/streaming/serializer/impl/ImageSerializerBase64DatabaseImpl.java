package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;

public class ImageSerializerBase64DatabaseImpl
        extends AbstractStreamingImageSerializer<File, ImageFrameMedia> {
    /**
     * {@inheritDoc}
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }

    /**
     * {@inheritDoc}
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(ImageFrameMedia media) throws IOException {
        return new Base64OutputStream(media.getEncodedImageOutput());
    }

    /**
     * {@inheritDoc}
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public final void serialize(File source, ImageFrameMedia media) throws IOException {
       long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            Writer writer = new StringWriter();
          //  writer.write(""+numberOfLines+";"+size+";");
            writer.write("HELLO");
            //writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                writer.write("\n");
            }
            writer.flush();
        }
    }

   /**public final void serialize(File source, ImageFrameMedia media) throws IOException {
       long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
       long size = Files.size(source.toPath());

      /** String file = Files.readString(source.toPath());
file = file.replaceAll("\n","").replaceAll("\r","");
Files.write(source.toPath(), file.getBytes()).toFile();**/
       /**try(OutputStream os = media.getEncodedImageOutput()) {
           Writer writer = new StringWriter();
           try(OutputStream eos = getSerializingStream(media)) {

               StringBuilder contentBuilder = new StringBuilder();
               BufferedReader br = new BufferedReader(new FileReader((source)));
                   String stringCurrent;
                   while((stringCurrent = br.readLine()) != null){
                       contentBuilder.append(stringCurrent);
                   }
               String sourceString = contentBuilder.toString();
               String stringModified = numberOfLines + ";" + size + ";" + sourceString;
               System.out.println("xxx" +stringModified);
               FileWriter fileWriter = new FileWriter(source);
               PrintWriter printWriter = new PrintWriter(fileWriter);
               printWriter.print(stringModified);
              // eos.write(stringModified.getBytes());
               getSourceInputStream(source).transferTo(eos);
               writer.write("\n");
           }
           writer.flush();

       }
       try(OutputStream os = media.getEncodedImageOutput()) {
           Writer writer = new StringWriter();
           //  writer.write(""+numberOfLines+";"+size+";");
           //writer.flush();
           try(OutputStream eos = getSerializingStream(media)) {

               getSourceInputStream(source).transferTo(eos);
               File mediaFile = ((File) media.getChannel());

               StringBuilder contentBuilder = new StringBuilder();
               BufferedReader br = new BufferedReader(new FileReader((mediaFile)));
               String stringCurrent;
               while((stringCurrent = br.readLine()) != null){
                   contentBuilder.append(stringCurrent);
               }
               String mediaString = contentBuilder.toString();
               String stringModified = numberOfLines + ";" + size + ";" + mediaString;
               System.out.println("xxx" +stringModified);
               eos.write(stringModified.getBytes());
               writer.write("\n");
           }
           writer.flush();

       }
   }**/

}
