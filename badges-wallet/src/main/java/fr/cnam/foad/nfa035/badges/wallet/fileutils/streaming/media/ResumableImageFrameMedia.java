package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Permet d'ajouter des images à la suite les unes des autres
 */
public interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T> {

    /**
     *Permet de récupérer unBufferedReader
     *
     * @param
     * @return
     * @throws IOException
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
