package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.BadgeDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;

public interface DatabaseDeserializer<M extends ImageFrameMedia> extends BadgeDeserializer<M> {
    /**
     * methode pour récupérer un inputStream à partir d'un String
     *
     * @param data
     * @return
     * @param <K>
     * @throws IOException
     */
    <K extends InputStream> K getDeserializingStream(String data) throws IOException;
}
